//
//  GifEditorViewController.swift
//  GifMaker_Swift_Template
//
//  Created by Daniel Peña Vazquez on 2/11/17.
//  Copyright © 2017 Gabrielle Miller-Messner. All rights reserved.
//

import UIKit
class GifEditorViewController: UIViewController {
    
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var captionTextField: UITextField!
    
    //var gifURL: NSURL? = nil
    var gif:Gif?
    @IBAction func presentPreview(_ sender: Any) {
        let previewVC =
            storyboard?.instantiateViewController(withIdentifier: "PreviewViewController") as!
        PreviewViewController
        self.gif?.caption=self.captionTextField.text
        let regift:Regift=Regift(sourceFileURL: (self.gif?.videoURL)!, frameCount: frameCount, delayTime: delayTime, loopCount: loopCount)
        let captionFont:UIFont = self.captionTextField.font!;
        
        let gifURL:NSURL = regift.createGif(caption: self.captionTextField.text, font: captionFont)!
        let newGif:Gif=Gif(url: gifURL, videoURL: self.gif!.videoURL, caption: self.captionTextField.text)
        previewVC.gif=newGif
        navigationController?.pushViewController(previewVC, animated: true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        textField.placeholder=""
    }
    func textFieldShouldReturn(textField:UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
        super.viewDidLoad()
        
        let defaultAttributes=[NSAttributedStringKey.font.rawValue:UIFont(name: "HelveticaNeue-CondensedBlack", size: 40.0)!]
        self.captionTextField.defaultTextAttributes=defaultAttributes
        self.captionTextField.textAlignment = .center
        self.captionTextField.attributedPlaceholder=NSAttributedString(string: "Add Caption")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        // Called when the view is about to made visible.
        super.viewWillAppear(animated)
        gifImageView.image=gif?.gifImage
        self.subscribeToKeyboardNotifications()
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
        super.viewWillDisappear(animated)
        self.unsubscribeFromKeyboardNotifications()
        self.title = ""
    }
    
    @objc func dismissKeyboard()
    {
        print ("ocultando teclado")
        self.view.endEditing(true)
    }
}

