//
//  AppDelegate.swift
//  GifMaker_Swift_Template
//
//  Created by Gabrielle Miller-Messner on 5/4/16.
//  Copyright © 2016 Gabrielle Miller-Messner. All rights reserved.
//

import UIKit

// MARK: - AppDelegate: UIResponder, UIApplicationDelegate
//@IBAction func launchVideoCamera(_ sender: Any) {}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: Properties
    
    var window: UIWindow?
    
    // MARK: UIApplicationDelegate
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool
    {
        // Override point for customization after application launch.
        return true
    }
    
}
