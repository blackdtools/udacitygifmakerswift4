//
//  Gif.swift
//  GifMaker_Swift_Template
//
//  Created by Daniel Peña Vazquez on 4/11/17.
//  Copyright © 2017 Gabrielle Miller-Messner. All rights reserved.
//

import UIKit
class Gif {
    var url: NSURL
    var videoURL: NSURL
    var caption: String?
    var gifImage: UIImage?
    var gifData: NSData?
    
    init(url:NSURL,videoURL: NSURL, caption: String?)
    {
        self.url = url
        self.videoURL = videoURL
        self.caption = caption
        self.gifImage = UIImage.gif(url:url.absoluteString!)
        self.gifData = nil
    }
    init (name: String) {
        self.gifImage = UIImage.gif(name:name)
        self.url=NSURL()
        self.videoURL=NSURL()
        self.caption=nil
        
    }
    
}
