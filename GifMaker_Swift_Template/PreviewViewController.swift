//
//  PreviewViewController.swift
//  GifMaker_Swift_Template
//
//  Created by Daniel Peña Vazquez on 2/11/17.
//  Copyright © 2017 Gabrielle Miller-Messner. All rights reserved.
//

import UIKit
class PreviewViewController: UIViewController {
    var gif:Gif?
    
    @IBOutlet weak var gifImageView: UIImageView!
    
    
    
    @IBAction func shareGif(sender: Any) {
        let url: NSURL = (self.gif?.url)!
        let animatedGIF:NSData! = NSData(contentsOf: (url as NSURL) as URL)
        let itemsToShare:[NSData] = [animatedGIF]
        let activityVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
        activityVC.completionWithItemsHandler = {(activity, completed, items, error) in
            if (completed) {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        navigationController?.present(activityVC, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        // Called when the view is about to made visible.
        super.viewWillAppear(animated)
        gifImageView.image=gif?.gifImage
    }
}

