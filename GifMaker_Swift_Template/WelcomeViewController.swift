//
//  WelcomeViewController.swift
//  GifMaker_Swift_Template
//
//  Created by Daniel Peña Vazquez on 2/11/17.
//  Copyright © 2017 Gabrielle Miller-Messner. All rights reserved.
//


import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var gifImageView: UIImageView!
    
    override func viewWillAppear(_ animated: Bool)
    {
        // Called when the view is about to made visible.
        super.viewWillAppear(animated)
        let proofOfConceptGif:UIImage? = UIImage.gif(name: "hotlineBling")
        gifImageView.image=proofOfConceptGif
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Called when the parent application receives a memory warning. On iOS 6.0 it will no longer clear the view by default.
    }
    
}
