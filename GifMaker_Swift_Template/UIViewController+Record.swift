//
//  UIViewController+Record.swift
//  GifMaker_Swift_Template
//
//  Created by Daniel Peña Vazquez on 2/11/17.
//  Copyright © 2017 Gabrielle Miller-Messner. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

// Regift constants
let frameCount: Int = 16
let delayTime: Float = 0.2
let loopCount: Int = 0 // 0 means loop forever

extension UIViewController {
    
 
    
    @IBAction func launchVideoCamera(sender: Any) {
        print ("launch click")
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            let recordVideoController:UIImagePickerController=UIImagePickerController()
            recordVideoController.sourceType=UIImagePickerControllerSourceType.camera
            recordVideoController.mediaTypes=[kUTTypeMovie as String]
            recordVideoController.allowsEditing=false
            recordVideoController.delegate=self
            present(recordVideoController,animated:true,completion:nil)
            
        }
        
    }
    
    
    
}

// MARK: - UIViewController: UINavigationControllerDelegate

extension UIViewController: UINavigationControllerDelegate {}

// MARK: - UIViewController: UIImagePickerControllerDelegate

extension UIViewController: UIImagePickerControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print ("image picked ok");
        let mediaType:String=info[UIImagePickerControllerMediaType] as! String
        if (mediaType == kUTTypeMovie as String) {
            let videoURL:NSURL = info[UIImagePickerControllerMediaURL] as! NSURL
            dismiss(animated:true,completion:nil)
            // UISaveVideoAtPathToSavedPhotosAlbum(videoURL.path!,nil,nil,nil)
               print ("doing dismiss");
            convertVideoToGIF(videoURL: videoURL)
           // performSegue(withIdentifier: "showGifEditor", sender: nil)
        }
    }
    
    public func imagePickerControllerDidCancel(picker:UIImagePickerController)
    {
        dismiss(animated:true,completion:nil)
    }
    
    // GIF conversion methods
    func convertVideoToGIF(videoURL: NSURL)
    {
        let regift = Regift(sourceFileURL: videoURL, frameCount: frameCount, delayTime: delayTime, loopCount: loopCount)
        let gifURL:NSURL? = regift.createGif()
        let gif=Gif(url: gifURL!, videoURL: videoURL, caption: nil)
        displayGIF(gif:gif)
    }
    func displayGIF(gif:Gif)
    {
        let gifEditorVC =
            storyboard?.instantiateViewController(withIdentifier: "GifEditorViewController") as!
        GifEditorViewController
        gifEditorVC.gif=gif
        navigationController?.pushViewController(gifEditorVC, animated: true)
    }
    
}

