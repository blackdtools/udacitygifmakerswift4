//
//  RegiftCaption.swift
//  GifMaker_Swift_Template
//
//  Created by Gabrielle Miller-Messner on 4/22/16.
//  Modified from http://stackoverflow.com/questions/6992830/how-to-write-text-on-image-in-objective-c-iphone
//

import UIKit
import Foundation
import CoreGraphics

extension Regift {
    
    
    func addCaption(image: CGImage, text: NSString, font: UIFont) -> CGImage {
        let image = UIImage(cgImage: image)
        
        // Text attributes
        let color = UIColor.white
        var attributes = [NSAttributedStringKey.foregroundColor:color, NSAttributedStringKey.font:font, NSAttributedStringKey.strokeColor : UIColor.black, NSAttributedStringKey.strokeWidth : -4] as [NSAttributedStringKey : Any]
        
        // Get scale factor
        let testSize:CGSize =  text.size(withAttributes: attributes)
        let scaleFactor = testSize.height/360
        
        // Apply scale factor to attributes
        let scaledFont: UIFont = UIFont(name: "HelveticaNeue-CondensedBlack", size:image.size.height * scaleFactor)!
        attributes[NSAttributedStringKey.font] = scaledFont
        
        // Text size
        let size:CGSize =  text.size(withAttributes: attributes)
        let adjustedWidth = ceil(size.width)
        let adjustedHeight = ceil(size.height)
        
        // Draw image
        UIGraphicsBeginImageContext(image.size)
        let rect_x:CGFloat=0
        let rect_y:CGFloat=0
        let rect_width:CGFloat=image.size.width
        let rect_height:CGFloat=image.size.height
        
        let firstRect = CGRect(x:rect_x,y:rect_y,width:rect_width,height:rect_height)
        image.draw(in: firstRect)
        
        // Draw text
        let sideMargin = (image.size.width - adjustedWidth)/2.0
        let bottomMargin = image.size.height/6.0
        
        let point_x:CGFloat=sideMargin
        let point_y:CGFloat=image.size.height - bottomMargin

        let textOrigin  = CGPoint(x: point_x, y: point_y)
        
        let rect2_x:CGFloat=textOrigin.x
        let rect2_y:CGFloat=textOrigin.y
        let rect2_width:CGFloat=adjustedWidth
        let rect2_height:CGFloat=adjustedHeight
        let secondRect = CGRect(x:rect2_x,y:rect2_y,width:rect2_width,height:rect2_height)
        text.draw(with: secondRect, options:.usesLineFragmentOrigin, attributes: attributes, context:nil)
        
        // Capture combined image and text
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.cgImage!
    }
}
